# Open iT Python Library

A library for reading the base datawarehouse.

## Install

```sh
pip install git+https://gitlab.com/ibilon.openit.com/pyopenit.git#egg=pyopenit
```

Or add the following to your `requirements.txt` file.

```
git+https://gitlab.com/ibilon.openit.com/pyopenit.git#egg=pyopenit
```

Or in your `setup.py` file.

```
pyopenit @ git+https://gitlab.com/ibilon.openit.com/pyopenit.git#egg=pyopenit
```

## How to Use

Read the database.

```python
import pandas
import openit

openit.data_dir = '/path/to/OpeniT/data'
date_range = pandas.date_range(start='2015-7', periods=1, freq='D')
df = openit.data(date_range, datatype=70)
print(df)
```

Read `licpoll2` archive.

```python
import pandas
import openit

openit.data_dir = '/path/to/OpeniT/data'
date_range = pandas.date_range(start='2015-7', periods=1, freq='D')
df = openit.data(date_range, archtype='licpoll2')
print(df)
```
