from setuptools import setup, find_packages

setup(
    name='pyopenit',
    version='0.13.0',
    packages=find_packages(),
    install_requires=[
        'fastparquet',
        'pyarrow',
        'pandas',
        'cchardet'
    ]
)
