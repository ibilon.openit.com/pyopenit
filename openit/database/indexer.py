from pathlib import Path
import openit
import pandas
import os

def index(database):
    datatypes = openit.datatypes.classifications.keys()
    dataframe = get_data_paths(database)
    dataframe = add_meta(dataframe)
    dataframe = dataframe[ dataframe['datatype'].isin(datatypes) ]
    dataframe = dataframe[ dataframe['mw'] == 'M' ]
    dataframe = add_datetime(dataframe)
    return dataframe

def get_data_paths(database):
    result = []
    for root, dirs, files in os.walk(database):
        for filename in files:
            filepath  = Path(os.path.join(root, filename))
            relpath   = Path(os.path.relpath(filepath, database))
            splitext  = os.path.splitext(filename)
            extension = splitext[1]
            if splitext[0] == 'data':
                entry = dict(filepath=filepath, relpath=relpath, filename=filename, extension=extension)
                result.append(entry)
    return pandas.DataFrame(result)

def infer_resolution(relpath):
    mapping = [-1, -1, -1, -1, 31104000, 2592000, 86400]
    length  = len(relpath.parts)
    assert length <= 7
    return int(relpath.parts[5]) if length >= 7 else mapping[length]

def infer_month(relpath):
    return int(relpath.parts[3]) if len(relpath.parts) >= 5 else 1

def infer_day(relpath):
    return int(relpath.parts[4]) if len(relpath.parts) >= 6 else 1

def add_meta(dataframe):
    dataframe['datatype']   = [relpath.parts[0]          for relpath in dataframe['relpath']]
    dataframe['mw']         = [relpath.parts[1]          for relpath in dataframe['relpath']]
    dataframe['year']       = [int(relpath.parts[2])     for relpath in dataframe['relpath']]
    dataframe['month']      = [infer_month(relpath)      for relpath in dataframe['relpath']]
    dataframe['day']        = [infer_day(relpath)        for relpath in dataframe['relpath']]
    dataframe['resolution'] = [infer_resolution(relpath) for relpath in dataframe['relpath']]
    return dataframe

def add_datetime(dataframe):
    dataframe['datetime'] = pandas.to_datetime(dataframe[ ['year', 'month', 'day'] ])
    return dataframe
