from . import datatypes, database, datafile, openitdatabase, licpoll2, parquet
from pathlib import Path
import pandas
import os

data_dir = os.environ['OPENIT_DATA'] if 'OPENIT_DATA' in os.environ else None

def ymdfiles(directory, date_range, filename):
    paths = []
    files = []
    if isinstance(date_range.freq, pandas.tseries.offsets.YearEnd):
        paths = [ ( '%s/%s' % (directory, d.year) ) for d in date_range ]
    elif isinstance(date_range.freq, pandas.tseries.offsets.MonthEnd):
        paths = [ ( '%s/%s/%s' % (directory, d.year, d.month) ) for d in date_range ]
    elif isinstance(date_range.freq, pandas.tseries.offsets.Day):
        paths = [ ( '%s/%s/%s/%s' % (directory, d.year, d.month, d.day) ) for d in date_range ]
    elif isinstance(date_range.freq, pandas.tseries.offsets.Hour):
        paths = [ ( '%s/%s/%s/%s/3600' % (directory, d.year, d.month, d.day) ) for d in date_range ]
    elif isinstance(date_range.freq, pandas.tseries.offsets.Second):
        paths = [ ( '%s/%s/%s/%s/%s' % (directory, d.year, d.month, d.day, date_range.freq.n) ) for d in date_range ]
    else:
        raise Exception('Unsupported freq in supplied date_range')
    for path in set(paths):
        files = files + list(Path(path).glob(filename))
    return files

def data(date_range, datatype=None, archtype=None, filename=None):
    if datatype != None:
        datatype        = str(datatype)
        assert datatype in datatypes.classifications, 'Unknown datatype: %s' % datatype
        database_dir    = Path(data_dir) / 'database' / datatype / 'M'
        accelerator_dir = Path(data_dir) / 'temp' / 'QueryAccelerator' / 'database' / datatype / 'M'
        files           = ymdfiles(accelerator_dir, date_range=date_range, filename='data.parquet')
        if files:
            return parquet.read_files(files)
        files     = ymdfiles(database_dir, date_range=date_range, filename='data*')
        dataframe = datafile.read_files(files, datatype)
        return dataframe
    elif archtype != None:
        directory = Path(data_dir) / 'archive' / archtype
        if filename is None:
            filename = '*.in.gz'
        files = ymdfiles(directory, date_range=date_range, filename=filename)
        if archtype == 'licpoll2':
            return licpoll2.read_files(files)
        else:
            raise Exception('Unsupported archtype')
    return None

def save(dataframe):
	return openitdatabase.save(dataframe, data_dir)
