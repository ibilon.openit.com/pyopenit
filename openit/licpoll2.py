import pathlib
import pandas
import gzip

columns = {}
columns['2.0'] = ['field_type', 'feature_type', 'product', 'feature', 'feature_version', 'avail_licenses', 'used_licenses', 'queued_licenses', 'user', 'tty', 'license_daemon', 'license_server', 'license_server_port', 'client_port', 'application_host', 'license_startup_time', 'entry_version', 'host_list']
columns['2.1'] = columns['2.0']
columns['2.2'] = ['field_type', 'feature_type', 'product', 'feature', 'feature_version', 'token_base', 'token_cost', 'avail_licenses', 'used_licenses', 'queued_licenses', 'user', 'tty', 'license_daemon', 'license_server', 'license_server_port', 'client_port', 'application_host', 'license_startup_time', 'entry_version', 'host_list']
columns['2.3'] = columns['2.2']
columns['2.4'] = ['field_type', 'feature_type', 'product', 'feature', 'feature_version', 'token_base', 'token_cost', 'ppu', 'avail_licenses', 'used_licenses', 'queued_licenses', 'user', 'tty', 'license_daemon', 'license_server', 'license_server_port', 'client_port', 'application_host', 'license_startup_time', 'entry_version', 'host_list']
columns['2.5'] = ['field_type', 'feature_type', 'product', 'feature', 'feature_version', 'token_base', 'token_cost', 'ppu', 'avail_licenses', 'used_licenses', 'queued_licenses', 'user', 'tty', 'license_daemon', 'license_server', 'license_server_port', 'client_port', 'application_host', 'license_startup_time', 'entry_version', 'host_list', 'linger']
columns['2.6'] = columns['2.5']
columns['2.7'] = ['field_type', 'feature_type', 'product', 'feature', 'feature_version', 'token_base', 'token_cost', 'ppu', 'avail_licenses', 'used_licenses', 'queued_licenses', 'user', 'tty', 'license_daemon', 'license_server', 'license_server_port', 'client_port', 'application_host', 'license_startup_time', 'entry_version', 'host_list', 'linger', 'licensing_model', 'license_model_epoch']

int_types        = {}
int_types['2.0'] = [1,2,6,7,8,16]
int_types['2.1'] = int_types['2.0']
int_types['2.2'] = [1,2,7,8,9,10,18]
int_types['2.3'] = int_types['2.2']
int_types['2.4'] = [1,2,7,8,9,10,11,19]
int_types['2.5'] = [1,2,7,8,9,10,11,19,22]
int_types['2.6'] = int_types['2.5']
int_types['2.7'] = [1,2,7,8,9,10,11,19,22,24]

def load(files):
    l = []
    for file in files:
        path    = pathlib.Path(file)
        openfn  = gzip.open if path.suffix == '.gz' else open
        f       = openfn(path, 'rt')
        lines   = f.readlines()
        l.append(lines)
        f.close()
    return l

def parse(objects):
    all_frames = []
    for lines in objects:
        # Parse a licpoll object file
        version     = ''
        frames      = []
        frame       = []
        interval    = 0
        time_list   = []
        for line in lines:
            # Remove trailing newlines
            line = line.strip()
            if len(line) > 1 and line[0] != '@':
                data = line.split(':')
                frame.append(data)
            elif line.find("@polling.start") != -1:
                frames.append(frame)
                frame = []
                time = int(line.split("=")[1])
                time_list.append(time)
            elif line.find("@polling.interval") != -1:
                interval = int(line.split("=")[1])
            elif line.find("@licpoll.log.version") != -1:
                version = line.split("=")[1]
        # Remove signature
        frame.pop()
        # Add the last frame
        frames.append(frame)
        # Remove first empty frame
        frames.pop(0)
        # Add version, interval, and time list
        frames.append(version)
        frames.append(interval)
        frames.append(time_list)
        # Add to all frames
        all_frames.append(frames)
    return all_frames

def filter(objects):
    for frames in objects:
        for i in range(0, len(frames) - 3):
            frame = frames[i]
            new_frame = []
            for j in range(0, len(frame)):
                data = frame[j]
                # Filter unecessary field types
                if int(data[0]) <= 2:
                    new_frame.append(data)
            frames[i] = new_frame
    return objects

def max_version(objects):
    versions = [ frames[-3] for frames in objects ]
    return max(versions)

def strip(objects):
    for frames in objects:
        for i in range(0, len(frames) - 3):
            frame = frames[i]
            for j in range(0, len(frame)):
                data = frame[j]
                for k in range(0, len(data)):
                    value = data[k]
                    data[k] = value.strip()
    return objects

def dtypes(objects):
    for frames in objects:
        version = frames[-3]
        indexes = int_types[version]
        for i in range(0, len(frames) - 3):
            frame = frames[i]
            for j in range(0, len(frame)):
                data = frame[j]
                for k in indexes:
                    if k <= len(data):
                        index = k - 1
                        value = data[index]
                        data[index] = 0 if value == "" else int(value)
    return objects

def mkdict(objects):
    for frames in objects:
        version = frames[-3]
        names   = columns[version]
        for i in range(0, len(frames) - 3):
            frame = frames[i]
            for j in range(0, len(frame)):
                data    = frame[j]
                d       = dict.fromkeys(names[0:len(data)], '')
                for k in range(0, len(data)):
                    d[names[k]] = data[k]
                frame[j] = d
    return objects

def mkindex(objects):
    for frames in objects:
        for i in range(0, len(frames) - 3):
            frame = frames[i]
            for data in frame:
                data['index_short']     = data['product'] + data['feature'] + data['feature_version']
                if 'user' in data:
                    data['index_long']  = data['product'] + data['feature'] + data['feature_version'] + data['user']
                else:
                    data['index_long']  = data['product'] + data['feature'] + data['feature_version']
    return objects

def carry(objects):
    for frames in objects:
        for i in range(1, len(frames) - 3):
            previous_frame  = frames[i-1]
            frame           = frames[i]
            short_indexes   = [ data['index_short'] for data in frame ]
            long_indexes    = [ data['index_long'] for data in frame ]
            new_frame       = [ data for data in previous_frame if not data['index_short'] in long_indexes and not data['index_long'] in short_indexes ] + frame
            frames[i]       = new_frame
    return objects

def copy(objects):
    for frames in objects:
        for i in range(1, len(frames) - 3):
            frame       = frames[i]
            frames[i]   = [ data.copy() for data in frame ]
    return objects

def add_time_interval(objects):
    for frames in objects:
        time_list   = frames[-1]
        interval    = frames[-2]
        for i in range(0, len(frames) - 3):
            frame   = frames[i]
            time    = time_list[i]
            for j in range(0, len(frame)):
                data                = frame[j]
                data['time']        = time
                data['interval']    = interval
        # Remove version, interval, and time list
        frames.pop()
        frames.pop()
        frames.pop()
    return objects

def remove_index(objects):
    for frames in objects:
        for i in range(0, len(frames)):
            frame = frames[i]
            for data in frame:
                if 'index_short' in data:
                    del data['index_short']
                    del data['index_long']
    return objects

def flatten(objects):
    return [ data for frames in objects for frame in frames for data in frame ]

def read_files(files):
    if not files:
        return pandas.DataFrame()
    objects = load(files)
    objects = parse(objects)
    objects = filter(objects)
    version = max_version(objects)
    objects = strip(objects)
    objects = dtypes(objects)
    objects = mkdict(objects)
    objects = mkindex(objects)
    objects = carry(objects)
    objects = copy(objects)
    objects = add_time_interval(objects)
    objects = remove_index(objects)
    dicts   = flatten(objects)
    df      = pandas.DataFrame(dicts, columns=['time', 'interval'] + columns[version])
    return df
