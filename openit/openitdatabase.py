from time import timezone
from . import datatypes
import pathlib
import pandas
import gzip

def getpath(row):
    interval = int(row['interval'])
    epoch = int(row['time'])
    time = pandas.to_datetime(epoch-timezone, unit='s')
    year = time.year
    month = time.month
    day = time.day
    if interval == 86400:
        path = '%s/%s/%s/data.gz' % (year, month, day)
    elif interval == 2592000:
        path = '%s/%s/data' % (year, month)
    elif interval == 31104000:
        path = '%s/data' % (year)
    else:
        raise Exception('unsupported resolution')
    return path

def format_row(row, classifications, measurements):
    c = list()
    m = list()
    x = list()
    for k in classifications:
        c.append(str(row[k]))
    for k in measurements:
        m.append(str(row[k]))
    d = 10 - (len(c) - 3)
    x.append(c[0])
    x.append(c[1])
    x.append(c[2])
    for i in range(3, len(c)):
        x.append('0')
        x.append(c[i])
    for i in range(0, d):
        x.append(' ')
        x.append(' ')
    l1 = ':'.join(x)
    l2 = ':'.join(m)
    return l1, l2

def dt(dataframe):
    return dataframe.at[0, 'datatype']

def cm(dataframe, datatype):
    c = datatypes.classifications[datatype]
    m = datatypes.measurements[datatype]
    return c, m

def map(dataframe):
    objects = dict()
    for index, row in dataframe.iterrows():
        path = getpath(row)
        if not path in objects:
            objects[path] = list()
        objects[path].append(row)
    return objects

def format(objects, classifications, measurements):
    new_objects = dict()
    for path in objects.keys():
        rows = objects[path]
        if not path in new_objects:
            new_objects[path] = list()
        for row in rows:
            l1, l2 = format_row(row, classifications, measurements)
            new_objects[path].append(l1)
            new_objects[path].append(l2)
    return new_objects

def write(objects, parent_path):
    for path in objects:
        filepath = pathlib.Path('%s/%s' % (parent_path, path))
        openfn = gzip.open if filepath.suffix == '.gz' else open
        with openfn(filepath, 'wb') as fp:
            for line in objects[path]:
                fp.write((line + '\n').encode('utf-8'))

def save(dataframe, data_dir):
    assert dataframe['interval'].unique().size == 1
    assert dataframe['datatype'].unique().size == 1
    datatype = dt(dataframe)
    c, m = cm(dataframe, datatype)
    objects = map(dataframe)
    objects = format(objects, c, m)
    write(objects, '%s/database/%s/M' % (data_dir, datatype))
