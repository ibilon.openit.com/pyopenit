import cchardet as chardet
from . import datatypes
import pathlib
import pandas
import gzip

def get_datatype(record):
    return record[2]

def torecord(line1, line2):
    c = line1.replace(': ', ':').replace(':0:', ':').split(':')
    m = line2.split(':')
    return c + m

def get_dtypes(classifications, measurements):
    dtypes = dict()
    for column in classifications + measurements:
        dtypes[column] = datatypes.dtypes[column]
    return dtypes

def trim(records, classifications, measurements):
    c = len(classifications)
    m = len(measurements)
    return [ record[0:c] + record[-m:] for record in records ]

def load(files):
    result   = []
    mode     = 'rt'
    encoding = None
    files    = [ pathlib.Path(filepath) for filepath in files ]
    for filepath in files:
        openfn = gzip.open if filepath.suffix == '.gz' else open
        try:
            with openfn(filepath, mode, encoding=encoding) as file:
                result.append(file.readlines())
        except UnicodeDecodeError:
            encoding = detect_encoding(filepath, openfn)
            with openfn(filepath, mode, encoding=encoding) as file:
                result.append(file.readlines())
    return result

def detect_encoding(filepath, openfn):
    with openfn(filepath, 'rb') as file:
        buffer = file.read()
    return chardet.detect(buffer)['encoding']

def parse(objects):
    all_records = []
    for lines in objects:
        records = []
        for i in range(0, len(lines), 2):
            line1 = lines[i].strip()
            line2 = lines[i+1].strip()
            if not line2:
                break
            records.append(torecord(line1, line2))
        all_records.append(records)
    return all_records

def flatten(objects):
    return [ record for records in objects for record in records ]

def add_dtypes(dataframe, dtypes):
    return fix_dtypes(dataframe.astype(dtypes, errors='ignore'), dtypes)

def fix_dtypes(dataframe, dtypes):
    for column, series in dataframe.iteritems():
        actual   = series.dtype
        expected = dtypes[column]
        if actual != expected:
            dataframe[column] = pandas.to_numeric(series, errors='coerce').fillna(0).astype(expected)
    return dataframe

def read_files(files, datatype):
    assert datatype in datatypes.classifications, 'Undefined classifications of datatype: %s' % datatype
    assert datatype in datatypes.measurements,    'Undefined measurements of datatype: %s'    % datatype
    classifications = datatypes.classifications[datatype]
    measurements    = datatypes.measurements[datatype]
    columns         = classifications + measurements
    dtypes          = get_dtypes(classifications, measurements)
    records         = []
    if files:
        objects = load(files)
        objects = parse(objects)
        records = flatten(objects)
        records = trim(records, classifications, measurements)
    return add_dtypes(pandas.DataFrame(records, columns=columns), dtypes)
