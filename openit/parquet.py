import pandas

def read_files(files):
    if not files:
        return pandas.DataFrame()
    dataframes = [pandas.read_parquet(filepath) for filepath in files]
    return pandas.concat(dataframes)
